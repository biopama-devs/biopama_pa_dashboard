
jQuery(document).ready(function($) {
  var allResults = {};
  // var ACPStatsURL = "https://api.biopama.org/api/protection_level/function/api_acp_stats/";
  // var RegionStatsURL = "https://api.biopama.org/api/protection_level/function/api_region_stats/";
  // var CountryListURL = "https://api.biopama.org/api/protection_level/function/api_country_list_stats/region=";
  // var RegionPACatsURL = "https://api.biopama.org/api/protection_level/function/api_region_iucn_cat/region_acp=";
  // var regionGovTypeURL = "https://api.biopama.org/api/protection_level/function/api_region_gov_type/region_acp=";
  // var regionDesigURL = "https://api.biopama.org/api/protection_level/function/api_region_desig/region_acp=";
  var ACPStatsURL = "https://api.biopama.org/api/protection_level/function/api_acp_stats/";
  //var RegionStatsURL = "https://akp-dev.biopama.org/serviceapi/da366a8a-13a1-4cd9-bb69-f66b67df35e4/region_acp/";
  //var CountryListURL = "https://akp-dev.biopama.org/serviceapi/b4d078cf-3688-4273-8c24-facf39b7864b/region_acp/";
  //var CountryListURL = "https://akp-dev.biopama.org/serviceapi/93e7b1d5-7051-48ff-8626-8fce1a51c9de/region_acp/";
  //var CountryListURL = "https://africa-knowledge-platform.ec.europa.eu/serviceapi/PAdashboardcountrystats/";
  //var RegionPACatsURL = "https://akp-dev.biopama.org/serviceapi/63570b52-1b14-4ab9-9ea1-3ea7dc693e67/region_acp/";
  //var regionGovTypeURL = "https://akp-dev.biopama.org/serviceapi/680587df-1d5a-482a-a0c4-b49712171b20/region_acp/";
  //var regionDesigURL = "https://akp-dev.biopama.org/serviceapi/b937ff1d-83a7-40e0-90ed-1862018ba701/region_acp/";


  var RegionStatsURL = "https://api.biopama.org/api/protection_level/function/api_region_stats/region_acp_codes=";
  var CountryListURL = "https://api.biopama.org/api/protection_level/function/api_country_stats/Region_acp=";
  var RegionPACatsURL = "https://api.biopama.org/api/protection_level/function/api_region_iucn_cat/region_acp_codes=";
  var regionGovTypeURL = "https://api.biopama.org/api/protection_level/function/api_region_gov_type/region_acp_codes=";
  var regionDesigURL = "https://api.biopama.org/api/protection_level/function/api_region_desig_eng/region_acp_codes=";

  var tableAttribution = $("#dashboard-table-citation").text();
  var fullAttribution = document.getElementById('dashboard-attribution');
  var cloneFullAttribution = fullAttribution.cloneNode(true);
  $('#pa-dashboard-description').append(cloneFullAttribution);
  var paDashboardLegend = {
      textStyle: {  
        color: biopamaGlobal.chart.colors.text                         
      },
      show: true,
      bottom: '0%',
  };
  var paDashboardTooltip = {
    show: true,
    trigger: 'item',
    formatter: "{d}%"
  };
  var paDashboardTooltipWithNumber = {
    show: true,
    trigger: 'item',
    formatter: "{c} ({d}%)"
  };
  
  $.each(biopamaGlobal.regions,function(idx,region){
    var thisRegion = {};
    if (region.id != 'ACP'){
      emptyCard(region);
      $.when(
        $.getJSON(CountryListURL+region.id,function(d){
          allResults[region.id] = d; 
        }),
        $.getJSON(RegionPACatsURL+region.id,function(d){
          allResults[region.id+"cats"] = d; 
        }),
        $.getJSON(RegionStatsURL,function(d){
          allResults.regions = d; 
        }),
      ).then(function() {
        thisRegion = getRegionStats(region.id)
        buildDashboardCharts(thisRegion);
        buildDashboardTable(region.id);
        updateColors();
      });
    } else {
      // $.when(
      //   $.getJSON(ACPStatsURL,function(d){
      //     allResults.acp = d[0]; 
      //   }),
      //   $.getJSON(RegionStatsURL,function(d){
      //     allResults.regions = d.data; 
      //   }),
      // ).then(function() {
      //   thisRegion = allResults.acp;
      //   buildDashboardCharts(thisRegion);
      //   updateColors();
      // });
    }
  });
    
  function updateColors(){
    $(".color-ter").css("color", biopamaPAColors.terrestrial);
    $(".color-mar").css("color", biopamaPAColors.marine);
    $(".color-cos").css("color", biopamaPAColors.costal);
    $(".color-all").css("color", biopamaPAColors.totalArea);
  }
  
  function getRegionStats(regionID){
    var regionArray = allResults.regions;
    var thisRegion =regionArray.find(obj => {
      return obj.region_acp === regionID
    });
    return thisRegion;
  }
  function buildDashboardCharts(region){
    console.log(region);
    if (!region.region_acp){
      
      region.region_acp = 'ACP';
    }
    var terAreaProtTotal = Number(region.reg_land_area_prot / 1000).toFixed(2);
    var marAreaProtTotal = Number(region.reg_mar_area_prot / 1000).toFixed(2);
    var areaProtTotal = Number(terAreaProtTotal) + Number(marAreaProtTotal);
    
    $( 'div#' + region.region_acp + '-area-pa-total' ).text(areaProtTotal.toFixed(2));
    $( 'div#' + region.region_acp + '-area-ter-pa-total' ).text(terAreaProtTotal);
    $( 'div#' + region.region_acp + '-area-mar-pa-total' ).text(marAreaProtTotal);
    $( 'div#' + region.region_acp + '-num-ter-pa-total' ).text(region.reg_terr_count);
    $( 'div#' + region.region_acp + '-num-mar-pa-total' ).text(region.reg_mar_count);
    $( 'div#' + region.region_acp + '-num-cos-pa-total' ).text(region.reg_costal_count);
    /* 
    ## Start Region Area Protected Terrestrial Area  ##
    */
    var chartValues = calcChartValues(region.reg_terr_prot_perc);
    var chartData = {
      colors: "twoTerrestrial",
      legend: paDashboardLegend,
      tooltip: paDashboardTooltip,
      series: [{
        type: 'pie', 
        radius: ['20%', '60%'],
        avoidLabelOverlap: true,
        data: [
          {value: chartValues[0], name: 'Terrestrial Protection'},
          {value: chartValues[1], name: 'Not Protected'},
        ] 
      }]
    };
    $().createNoAxisChart('#' + region.region_acp + '-pie-chart-t', chartData); 
    $( '#' + region.region_acp + '-pie-chart-value-t' ).text(chartValues[0]+'%');
    /* 
    ## End Region Area Protected Terrestrial Area  ##
    */
    /* 
    ## Start Region Area Protected Marine Area  ##
    */
    var chartValues = calcChartValues(region.reg_mar_prot_perc)
    var chartData = {
      colors: "twoMarine",
      legend: paDashboardLegend,
      tooltip: paDashboardTooltip,
      series: [{
        type: 'pie', 
        radius: ['20%', '60%'],
        avoidLabelOverlap: true,
        data: [
          {value: chartValues[0], name: 'Marine Protection'}, 
          {value: chartValues[1], name: 'Not Protected'},
        ] 
      }]
    }
    $().createNoAxisChart('#' + region.region_acp + '-pie-chart-m', chartData); 
    $( '#' + region.region_acp + '-pie-chart-value-m' ).text(chartValues[0]+'%');
    /* 
    ## End Region Area Protected Marine Area  ##
    */
  };
  function calcChartValues(chartValue){
    var chartValues = [];
    chartValue = Number(chartValue).toFixed(2);
    chartValues.push(chartValue);
    var missingValue = 100 - chartValue;
    chartValues.push(missingValue);
    return chartValues;
  }
  function buildDashboardTable(regionID){
    var dataSet = allResults[regionID]; 
    //number of terrestrial PAs, number of marine PAs, number of costal PAs, terrestrial protection (km2), marine protection (km2), terrestrial protection (%), marine protection (%)
    var columnData = [
      {
        title: "ISO2",
        data: "iso2",
        "visible": false,
        "searchable": true
      },{ 
        title: "Country",
        data: "country_name",
        //responsivePriority: 0
      },{ 
        title: "Country Area",
        data: "country_area_km",
        //responsivePriority: 0
      },{ 
        title: "Total Number of PAs",
        data: "count",
        //responsivePriority: 1
      },{ 
        title: "Number of Terrestrial PAs",
        data: "terrestrial_count",
        //responsivePriority: 2,
        //className: "min-desktop",
      },{ 
        title: "Number of Marine PAs",
        data: "marine_count",
        //responsivePriority: 2
        //className: "min-desktop",
      },{ 
        title: "Number of Costal PAs",
        data: "costal_count",
        //responsivePriority: 2
        //className: "min-desktop",
      },{ 
        title: "Terrestrial Protection (km2)",
        data: "prot_km",
        //responsivePriority: 0
      },{ 
        title: "Marine Protection (km2)",
        data: "prot_mar_km",
        //responsivePriority: 0
      },{ 
        title: "Terrestrial Protection (%)",
        data: "prot_perc",
        //responsivePriority: 0
      },{ 
        title: "Marine Protection (%)",
        data: "prot_mar_perc",
        //responsivePriority: 0 
      },
    ];
    var columnSettings = [{ "className": regionID + "-cell region-cell", "targets": "_all" },
    { 
      "targets": [ 1 ],
      "createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
        $(cell).html('<a class"dashboard-country-link" href="/ct/country/'+rowData.iso2+'">'+rowData["country_name"]+'</a>');
      }
    }];
    var tableData = {
      columns: columnData,
      columnDefs: columnSettings,
      data: dataSet,
      attribution: tableAttribution,
      isComplex: true,
      pagination: false,
      fnFooterCallback: function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
        var thisRegion = getRegionStats(regionID);
        //var api = this.api();
        var nCells = nRow.getElementsByTagName('th');
        console.log(nRow);
        nCells[1].innerHTML = (parseFloat(thisRegion.reg_land_area).toFixed(2) / 1000).toFixed(2) + ' 1000km<sup>2</sup>';
        var footer = '<td>' + aaData.length + ' countries</td>' +
        '<td>' + (parseFloat(thisRegion.reg_land_area).toFixed(2) / 1000).toFixed(2) + ' 1000km<sup>2</sup></td>'+ //Area of Country
        '<td>' + thisRegion.count_reg_tot + ' PAs</td>'+ //# of PAs
        '<td>' + thisRegion.reg_terr_count + ' PAs</td>'+ //# of PAs
        '<td>' + thisRegion.reg_mar_count + ' PAs</td>'+ //# of PAs
        '<td>' + thisRegion.reg_costal_count + ' PAs</td>'+ //# of PAs
        '<td>' + (parseFloat(thisRegion.reg_land_area_prot).toFixed(2) / 1000).toFixed(2) + ' 1000km<sup>2</sup></td>'+//Area Terrestrial Protected
        '<td>' + (parseFloat(thisRegion.reg_mar_area_prot).toFixed(2) / 1000).toFixed(2) + ' 1000km<sup>2</sup></td>'+//Area Marine Protected
        '<td>' + parseFloat(thisRegion.reg_terr_prot_perc).toFixed(2) + ' %</td>'+//% Terrestrial Protected
        '<td>' + parseFloat(thisRegion.reg_mar_prot_perc).toFixed(2) + ' %</td>';//% Marine Protected
        console.log(footer);
        //$( api.table().footer() ).html(footer);
        
      }
    }
    $().createDataTable(regionID, tableData); 
  }
  function emptyCard(region){
    var regionHeader = '<h1>' + region.name + ' coverage</h1>';
    var regionTable = '<div class="card-body">'+
      '<table id="' + region.id + '" class="table biopama-table" style="width:100%"><thead></thead><tbody></tbody><tfoot></tfoot></table>'+
    '</div>';
    const card = document.getElementById('ACP-card');
    const newCard = card.cloneNode(true);
    newCard.id = region.id +'card';
    // hidden
    $(newCard).find(".region-header").html(regionHeader);
    $(newCard).find("#ACP-accordion").removeClass("hidden");
    $(newCard).find(".region-table").html(regionTable);
    $(newCard).find(".region-id-change").each(function( index ) {
      var thisID = $( this ).attr("id");
      var newID = thisID.substring(3);
      newID = region.id + newID;
      this.id = newID;
    });
    $(newCard).find(".region-target-change").each(function( index ) {
      var thisID = $( this ).attr("id");
      var newID = thisID.substring(7);
      newID = region.id + newID;
      $( this ).attr("data-bs-target",'#'+newID);
      $( this ).attr("aria-controls",newID);
    });
    $('#pa-dashboard-cards').append(newCard);
    $('#'+region.id+'-collapse-IUCN-Cats').on('shown.bs.collapse', function () {
      var regionData;
      if (!$(this).hasClass('dashboard-data-loaded')) {
        $.when(
          $.getJSON(RegionPACatsURL+region.id,function(d){
            regionData = d; 
          })
        ).then(function() {
          buildCatPieChart(region.id, regionData);
          $(this).addClass('dashboard-data-loaded');
        });
      }
    });
    $('#'+region.id+'-collapse-governance-types').on('shown.bs.collapse', function () {
      var regionData;
      if (!$(this).hasClass('dashboard-data-loaded')) {
        $.when(
          $.getJSON(regionGovTypeURL+region.id,function(d){
            regionData = d; 
          })
        ).then(function() {
          buildGovPieChart(region.id, regionData);
          $(this).addClass('dashboard-data-loaded');
        });
      }
    });
    $('#'+region.id+'-collapse-designations').on('shown.bs.collapse', function () {
      var regionData;
      if (!$(this).hasClass('dashboard-data-loaded')) {
        $.when(
          $.getJSON(regionDesigURL+region.id,function(d){
            regionData = d; 
          })
        ).then(function() {
          buildDesigTable(region.id, regionData);
          $(this).addClass('dashboard-data-loaded');
        });
      }
    });
    return;
  }
  function buildCatPieChart(regionID, regionData){
    console.log(regionData)
    //assign order to the results
    $.each(regionData,function(idx,cat){
      switch (cat.gov_type) {
        case "Ia":
          cat.order = 1;
          break;
        case "Ib":
          cat.order = 2;
          break;
        case "II":
          cat.order = 3;
          break;
        case "III":
          cat.order = 4;
          break;
        case "IV":
          cat.order = 5;
          break;
        case "V":
          cat.order = 6;
          break;
        case "VI":
          cat.order = 7;
          break;
        case "Not Assigned":
          cat.order = 8;
          break;
        case "Not Applicable":
          cat.order = 9;
          break;
        case "Not Reported":
          cat.order = 10;
          break;
      };
    });
    var sortedRegionCats = regionData.sort( $().sortObject("order", "asc") );
    var chartSeriesData = [];
    $.each(sortedRegionCats,function(idx,obj){
      var thisObj = {};
      thisObj.value = obj.sum;
      thisObj.name = obj.gov_type;
      chartSeriesData.push(thisObj);
    });
    var chartData = {
      legend: paDashboardLegend,
      tooltip: paDashboardTooltipWithNumber,
      series: [{
        type: 'pie', 
        bottom: '25%' ,
        radius: ['20%', '60%'],
        avoidLabelOverlap: true,
        data: chartSeriesData
      }]
    };
    $().createNoAxisChart('#' + regionID + '-pie-chart-cat', chartData); 
  }
  function buildGovPieChart(regionID, regionData){
    var sortedRegionData = regionData.sort( $().sortObject("gov_type", "asc") );
    var chartSeriesData = [];
    $.each(sortedRegionData,function(idx,obj){
      var thisObj = {};
      thisObj.value = obj.sum;
      thisObj.name = obj.gov_type;
      chartSeriesData.push(thisObj);
    });
    var chartData = {
      legend: paDashboardLegend,
      tooltip: paDashboardTooltipWithNumber,
      series: [{
        type: 'pie', 
        bottom: '25%' ,
        radius: ['45%', '50%'],
        avoidLabelOverlap: true,
        data: chartSeriesData
      }]
    };
    $().createNoAxisChart('#' + regionID + '-pie-chart-gov', chartData); 
  }
  function buildDesigTable(regionID, regionData){
    console.log(regionData);
    //number of terrestrial PAs, number of marine PAs, number of costal PAs, terrestrial protection (km2), marine protection (km2), terrestrial protection (%), marine protection (%)
    var columnData = [
      {
        title: "Designation",
        data: "desig_eng",
      },{ 
        title: "Total",
        data: "sum",
      }
    ];
    var columnSettings = [{ "className": regionID + "-cell region-cell", "targets": "_all" }];
    var tableData = {
      columns: columnData,
      columnDefs: columnSettings,
      data: regionData,
      attribution: tableAttribution,
      isComplex: true,
      pagination: true,
    }
    $().createDataTable(regionID+"-table-desig", tableData);
    // $("#"+regionID+"-table-desig").DataTable().order([0, 'desc']).draw();
    // console.log("test");
  }
});